#! /usr/bin/env python

"""String normalization functions."""
import re
import typing
import unicodedata as ud


class TextNormaliser:
    def __init__(
        self,
        spaces: typing.List[str] = [r"\s+", r"\u00a0"],
        dashes: typing.List[str] = [r"\u2012", r"\u2013", r"\u2014", r"\u2500"],
        single_quotes: typing.List[str] = [r"\u2018", r"\u2019", r"\u2039", r"\u203a"],
        double_quotes: typing.List[str] = [r"\u201c", r"\u201d", r"\u00ab", r"\u00bb"],
        unicode_nf: str = "NFC",
    ):
        self.spaces = spaces
        self.dashes = dashes
        self.single_quotes = single_quotes
        self.double_quotes = double_quotes
        self.unicode_nf = unicode_nf

        self.pipeline = [
            self.normalise_whitespace,
            self.normalise_dashes,
            self.normalise_single_quotes,
            self.normalise_double_quotes,
            self.normalise_unicode,
        ]

    def _sub(self, pat: typing.List[str], sub: str, text: str) -> str:
        return re.sub(r"|".join(pat), sub, text)

    def normalise_whitespace(self, text: str) -> str:
        """Replace two or more subsequent whitespaces, and non-breaking spaces,
        with a single space."""
        return self._sub(self.spaces, " ", text)

    def normalise_dashes(self, text: str) -> str:
        """Replace various dashes with minus."""
        return self._sub(self.dashes, "-", text)

    def normalise_single_quotes(self, text: str) -> str:
        """Replace left and right single quotation marks and guilemets with apostrophes."""
        return self._sub(self.single_quotes, "'", text)

    def normalise_double_quotes(self, text: str) -> str:
        """Replace left and right double quotation marks and guilemets with 'normal' quotation marks."""
        return self._sub(self.double_quotes, '"', text)

    def normalise_unicode(self, text: str) -> str:
        return ud.normalize(self.unicode_nf, text)

    def normalise_all(self, text: str) -> str:
        """Normalization pipeline. Applies the methods in this class."""
        for fun in self.pipeline:
            text = fun(text)
        return text


def removeprefix(s: str, prefix: str) -> str:
    """If the string starts with the prefix string, return string[len(prefix):]. Otherwise, return a copy of the original string."""

    if s.startswith(prefix):
        return s[len(prefix) :]
    return s


def removesuffix(s: str, suffix: str) -> str:
    """If the string ends with the suffix string, return string[:-len(prefix)]. Otherwise, return a copy of the original string."""

    if s.endswith(suffix):
        return s[: -len(suffix)]
    return s

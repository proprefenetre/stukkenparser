Stukkenparser
=============

NB: de actieve versie van dit project staat hier: https://gitlab.com/minfin-bbh/stukkenparser

Stukkenparser is een python package waarmee er structuur kan worden aangebracht
in begrotingsstukken die gepubliceerd zijn op `officielebekendmakingen.nl
<https://zoek.officielebekendmakingen.nl>`_.

De html van de officiele publicaties is niet gestructureerd volgens het
`Document Object Model <https://en.wikipedia.org/wiki/Document_Object_Model>`_
(DOM), maar bestaat uit een min of meer platte lijst van html-elementen. Door de
DOM-structuur aan te brengen in de documenten worden ze geschikt om te gebruiken
als data bron voor o.a. text mining, semantic lifting, etc.

.. note:: Stukkenparser is geschreven voor en getest op begrotingsstukken. Als het goed is werkt het voor alle documenten die gepubliceerd zijn op officielebekendmakingen.nl, maar dat is niet getest.


Installatie
-----------

De package kan geïnstalleerd worden vanaf gitlab. Dit kan met `Poetry <https://python-poetry.org/docs/>`_::

  $ poetry new <project_dir>
  $ cd <project_dir>
  $ poetry add git+https://gitlab.com/minfin-bbh/stukkenparser

Of gebruik pip::

  $ mkdir <project_dir>
  $ cd <project_dir>
  $ python -m venv .venv
  $ source .venv/bin/activate
  $ pip install git+https://gitlab.com/minfin-bbh/stukkenparser

Zie de `documentatie <https://stukkenparser.rtfd.io>`_ voor een beschrijving van de API en voorbeelden.

Voorbeelden
-----------

* `web app <https://gitlab.com/proprefenetre/stukkenparser-app>`_ 
* `cli <https://gitlab.com/proprefenetre/stukkenparser-cli>`_

Configuratiebestanden
---------------------

* `stukkenparser-config <https://gitlab.com/proprefenetre/stukkenparser-config>`_

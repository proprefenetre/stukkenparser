.. Stukkenparser documentation master file, created by
   sphinx-quickstart on Thu Nov 26 10:07:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Inhoud:

   self
   parsing
   configuration
   serialisation
   api

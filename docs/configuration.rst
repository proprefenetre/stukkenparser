.. _config-ref:

Configuratie
=============

De opmaak van de begrotingsstukken kan per hoofdstuk verschillen. Over het
algemeen wordt de RBV gevolgd, maar de voorschriften geven alleen aan welke er
in een stuk aanwezig moeten zijn. Hoe die vervolgens worden ingevuld staat de
auteur vrij. Een resultaat hiervan is dat het in sommige gevallen moeilijk is om
te bepalen hoe de secties in elkaar ingebed zijn.

Embedding
---------

Het is mogelijk om de parser een configuratie mee te geven waarin de *levels of
embedding* nader worden gespecificeerd. Dit kan aan de hand van de opmaak of de
tekstuele inhoud van een onderdeel.

Opmaak
^^^^^^

De opmaak van een element is gespecificeerd in de css class. In het algemeen
wordt gebruik gemaakt van de volgende stijlen:

1. **tussenkop_vet**: dikgedrukte tekst
2. **tussenkop_cur**: schuingedrukte tekst
3. **tussenkop_ondlijn**: onderstreepte tekst
4. **tussenkop_rom**: 'gewone' tekst

Meestal maakt men gebruik van deze volgorde, maar sommige hoofdstukken
(e.g. VWS) wijken er vanaf. De levels kunnen op de volgende manier worden
geconfigureerd:

Tekstuele inhoud
^^^^^^^^^^^^^^^^

Opmaak is niet altijd voldoende. Niveaus kunnen met meer precisie worden
toegekend door gebruik te maken van regular expressions die de matchen met de
koppen of tussenkoppen. E.g.::

    {
        "embedding": {
            "tussenkop": {
                "titel": {
                    "Ad \\d+[a-z]": 1.5,
                    "Paragraaf \\d\\.": 2.5,
                    "Uitgaven": 1.5
                }
            }
        }
    }

Deze configuratie specificeerd dat onderdelen met tekst in de vorm van "Ad 12"
een level 1.5 krijgen. "Paragraaf 1." krijgt niveau 2.5, etc.

.. note:: De niveaus hebben geen speciale semantische betekenis. Ze worden enkel gebruikt om de onderdelen van het document te ordenen. Als het dus helpt om een onderdeel niveau 10 te geven is dat geen probleem.

Types
-----

Ieder onderdeel heeft een type dat relevant is binnen het
begrotingsstuk. Standaard is het type gelijk aan het soort
onderdeel. I.e. een Alinea heeft type "alinea", Lijst heeft type "lijst", Tabel
heeft type "tabel". Met behulp van regular expressions kunnen de onderdelen
specifiekere types toegewezen krijgen. Hierdoor wordt het mogelijk om
bijvoorbeeld de structuur van de RBV aan te geven. Een voorbeeld: ::

    ...
    "types": {
        "kop": {
            "(?i)(?:[a-z]\\.) algemeen": "algemeen",
            "(?i)beleidsverslag": "beleidsverslag",
            "(?i)beleidsprioriteiten": "beleidsprioriteiten",
            "(?i)\\d\\.? beleidsartikel(en)?": "beleidsartikelen",
            "(?i)niet-beleidsartikel(en)?": "niet-beleidsartikelen",
            "(?i)bedrijfsvoeringsparagraaf": "bedrijfsvoeringsparagraaf",
            "(?i)jaarrekening": "jaarrekening",
            "(?i)leeswijzer": "leeswijzer",
            "(?i)beleidsagenda": "beleidsagenda",
            "(?i)bijlage\\b": "bijlage",
            "(?i)bijlagen": "bijlagen"
        },
        "tussenkop": {
            "(?i)beleidsartikel(en)?": "beleidsartikelen",
            "(?i)niet-beleidsartikel(en)?": "niet-beleidsartikelen",
            "(?i)bedrijfsvoeringsparagraaf": "bedrijfsvoeringsparagraaf",
        },
        "tabel": {
            "(?i)budgettaire gevolgen": "b_tabel"
        }
    },
    ...

Gebruik
-------

Configuratie mag de volgende keys bevatten:

* "embedding"

  * "kop", "tussenkop", "margetekst": embedding is alleen relevant voor `Kop`, `Tussenkop` en `Margetekst`

    * "opmaak": bepaalt level a.d.h.v. css class
    * "titel": bepaalt level a.d.h.v. regular expressions over de titel

* "types"

  * "kop", "tussenkop", "tabel", "<onderdeel>": alle subclasses van `Onderdeel` kunnen een type toegewezen
    krijgen. NB. dat de resultaten voor onderdelen die niet gespecificeerd zijn ook niet gedefinieerd zijn.
    

Configuratie kan zowel per instance van `Parser` als per document worden
gespecificeerd. Hierdoor kunnen eenvoudig kleine wijzigingen worden meegegeven
voor hoofdstukken die afwijken van de norm::

  import stukkenparser

  global_config = {
    "embedding": {
        "tussenkop": {
            "opmaak": {
                "tussenkop_vet": "2",
                "tussenkop_cur": "3",
                "tussenkop_ondlijn": "4",
                "tussenkop_rom": "5"
            }
        }
    },
    "types": {
        "kop": {
            "(?i)\\d\\.? beleidsartikel(en)?": "beleidsartikelen",
            "(?i)niet-beleidsartikel(en)?": "niet-beleidsartikelen",
        },
        "tabel": {
            "(?i)budgettaire gevolgen": "b_tabel"
        }
    },
  }

  parser = stukkenparser.Parser(config)  # e.g. voor VWS

  local_config = {
      "embedding": {
          "tussenkop": {
              "opmaak": {
                  "tussenkop_vet": "3",
                  "tussenkop_cur": "2"
               }
           }
      }
  }

  stuk = parser.parse(Path("kst-34567-XIV-1.html"), local_config)
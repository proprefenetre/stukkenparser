Parsen
======

Het parsen van de documenten gebeurt in twee stappen. De `Lexer` tokeniseert de html en geeft de tokens een type uit
onderstaand tabel.

.. list-table:: Tokens
  :header-rows: 1

  * - Type
    - Token
  * - Alinea
    - <p>
  * - Lijst
    - <ul>
  * - Item
    - <li>
  * - Figuur
    - <div class=“plaatje”>
  * - Opsomming
    - <div class=“alineagroep”>
  * - Tabel
    - <table>
  * - Rij
    - <th>, <tr>
  * - Cel
    - <td>
  * - Margetekst
    - <div class=“margetekst”>
  * - Tussenkop
    - <p class=“tussenkop”>
  * - Kop
    - <h1..n>

De parser consumeert de token stream en brengt er structuur in aan met behulp van de regels in onderstaande 'grammatica'.

.. productionlist::
   Sectie: ( <empty> | `Content` | `Paragraaf` | `Sectie` )+
   Paragraaf: ( <empty> | `Content` | `Paragraaf` )+
   Content: ( `Alinea` | `Lijst` | `Tabel` | `Figuur` | `Opsomming` )+
   Tabel: `Rij`+
   Rij: `Cel`+
   Cel: <terminal>
   Opsomming: ( `Alinea` | `Lijst` )+
   Lijst: `Item`+
   Item: ( `Alinea` | `Lijst` )
   Alinea: <terminal>

|

Gebruik
-------

..
   .. literalinclude:: ../examples/example.py
      :language: python
      :linenos:

Het laden en parsen van een document is betrekkelijk eenvoudig: 

.. code-block:: python

    from pathlib import Path

    import stukkenparser

    parser = stukkenparser.Parser()

    # Lees de html en parse het stuk
    stuk = parser.parse(Path("kst-34567-A-1.html"))


De `parse()`-method geeft `Stuk`-object terug. Deze class heeft een aantal methods waarmee je over de boom kunt itereren
of waarmee je de boom kunt doorzoeken.

.. code-block:: python

    # itereren
    for o in stuk.onderdelen():
        print(o.text)

    # zoeken
    lege_onderdelen = stuk.find(lambda o: hasattr(o, "children") and len(o) == 0)
    artikelen = stuk.find_type("artikel")
    keywords = stuk.find_string("infrastructuur", case=False)

Met behulp van de `XMLAdapter`-class kan de parse tree omgezet worden naar XML of RDF:

.. code-block:: python
    
    adapter = stukkenparser.XMLAdapter(stuk)

    with open("kst-34567-A-1.xml", "w", encoding="utf-8") as f:
        f.write(adapter.to_string())

.. note:: Na één keer parsen klopt de structuur van de parse tree waarschijnlijk niet. Met behulp van extra
          :ref:`config-ref` kan de boomstructuur gecorrigeerd worden.


Alternatieve lexers
-------------------

Het is mogelijk om alternatieve lexers te schrijven, zo lang de tokens maar hetzelfde blijven. Als je bijvoorbeeld
alleen geinteresseerd bent in tabellen kun je de volgende lexer gebruiken.

.. code-block:: python

    class TableLexer(Lexer):

        def _div_tokens(self) -> Iterator[Onderdeel]:
            """Tokenizes the document into Onderdelen."""
            divs = self.tree.xpath(".//div[@id='broodtekst']/div")
            for div in divs:
                for elem in div:
                    elif elem.tag == "div":
                        for elt in elem.iterchildren():
                            elif elt.tag == "table":
                                yield Tabel(elt, self.config)
                            else:
                                continue

        def _stream(self) -> Iterator[Onderdeel]:
            yield from self._div_tokens()


   parser = stukkenparser.Parser(lexer=TableLexer)
   parser.parse(stuk)

|

